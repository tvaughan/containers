# -*- coding: utf-8; mode: makefile-gmake; -*-

MAKEFLAGS += --warn-undefined-variables

SHELL := bash
.SHELLFLAGS := -euo pipefail -c

HERE := $(shell cd -P -- $(shell dirname -- $$0) && pwd -P)

.PHONY: all
all: build

.PHONY: is-defined-%
is-defined-%:
	@$(if $(value $*),,$(error The environment variable $* is undefined))

.PHONY: login
login: is-defined-GITLAB_USERNAME is-defined-GITLAB_PASSWORD
	@echo "$(GITLAB_PASSWORD)" | podman login -u $(GITLAB_USERNAME) --password-stdin $(REGISTRY)

.PHONY: build
build: login
	@podman build -t $(REGISTRY)/$(CONTAINER):$(VERSION) -f Containerfile .

.PHONY: push
push: build
	@podman push $(REGISTRY)/$(CONTAINER):$(VERSION)

.PHONY: rm
rm:
	@podman image rm $(REGISTRY)/$(CONTAINER):$(VERSION)

.PHONY: push-rm
push-rm: push rm

.PHONY: pull
pull: login
	@podman pull $(REGISTRY)/$(CONTAINER):$(VERSION)
