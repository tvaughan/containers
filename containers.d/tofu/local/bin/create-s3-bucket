#!/usr/bin/env bash
# -*- mode: sh; coding: utf-8; tab-width: 4; indent-tabs-mode: nil -*-
set -euo pipefail
IFS=$'\n\t'
errcode=0
readonly script="$(readlink -f "$0")"

function on_exit {
    errcode=${1:-$errcode}
    exit $errcode
}

trap on_exit EXIT

function on_error {
    errcode=$1
    linenum=$2
    echo "[ERROR] script: $script errcode: $errcode linenum: $linenum"
}

trap 'on_error $? $LINENO' ERR

function usage {
    cat <<__END_OF_USAGE__
Usage: $(basename $script) <region> <bucket name>

The environment variables AWS_ACCESS_KEY_ID and AWS_SECRET_ACCESS_KEY must
also be defined.
__END_OF_USAGE__
}

readonly USAGE=$(usage)

if [[ -z ${AWS_ACCESS_KEY_ID:-""} || -z ${AWS_SECRET_ACCESS_KEY:-""} ]]
then
    echo "[ERROR]: AWS environment variables are not defined."
    echo "$USAGE"
    on_exit 1
fi

readonly REGION=${1:-""}
readonly BUCKET=${2:-""}

if [[ -z $REGION || -z $BUCKET ]]
then
    echo "[ERROR]: Command-line options are incorrect."
    echo "$USAGE"
    on_exit 1
fi

if ! aws s3api --region=$REGION head-bucket --bucket $BUCKET > /dev/null 2>&1
then
    aws s3 --region=$REGION mb s3://$BUCKET
fi

on_exit 0
