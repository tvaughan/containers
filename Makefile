# -*- coding: utf-8; mode: makefile-gmake; -*-

MAKEFLAGS += --warn-undefined-variables

SHELL := bash
.SHELLFLAGS := -euo pipefail -c

HERE := $(shell cd -P -- $(shell dirname -- $$0) && pwd -P)

REGISTRY := registry.gitlab.com/tvaughan/containers

CONTAINERS ?= gitlab-ci-builder                                                 \
fedora                                                                          \
  erlang                                                                        \
    elixir                                                                      \
  openjdk-jre                                                                   \
    openjdk-jdk                                                                 \
      clojure                                                                   \
  ruby                                                                          \
    rails                                                                       \
  tofu                                                                          \
#

.PHONY: all
all: build

.PHONY: is-defined-%
is-defined-%:
	@$(if $(value $*),,$(error The environment variable $* is undefined))

.PHONY: login
login: is-defined-GITLAB_USERNAME is-defined-GITLAB_PASSWORD
	@echo "$(GITLAB_PASSWORD)" | podman login -u $(GITLAB_USERNAME) --password-stdin $(REGISTRY)

.PHONY: build push rm push-rm pull
build push rm push-rm pull: login is-defined-CONTAINERS
	@for CONTAINER in $(CONTAINERS); do                                     \
	  $(MAKE) -C containers.d/$$CONTAINER $@;                               \
	done;

.PHONY: gitlab-runner-%
gitlab-runner-%: is-defined-GITLAB_PASSWORD
	@gitlab-runner exec docker                                              \
	    --docker-privileged                                                 \
	    --docker-volumes /var/run/docker.sock:/var/run/docker.sock          \
	    --env CI_BUILD_TOKEN="$(GITLAB_PASSWORD)"                           \
	    --timeout 7200                                                      \
	    $*
